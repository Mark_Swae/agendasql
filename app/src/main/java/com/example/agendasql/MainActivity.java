package com.example.agendasql;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import database.AgendaContactos;
import database.AgendaDbHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {
    private AgendaContactos db;
    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox chkFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    private Contacto savedContact;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new AgendaContactos(MainActivity.this);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTel1 = (EditText) findViewById(R.id.txtTel1);
        txtTel2 = (EditText) findViewById(R.id.txtTel2);
        txtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        chkFavorito = (CheckBox) findViewById(R.id.cbFavorito);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnLista = (Button) findViewById(R.id.btnLista);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });

        btnLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                Bundle bundle = new Bundle();
                startActivityForResult(intent, 0);
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validar() == true){

                    Contacto contacto = new Contacto();
                    contacto.setNombre(txtNombre.getText().toString());
                    contacto.setDomicilio(txtDomicilio.getText().toString());
                    contacto.setNotas(txtNotas.getText().toString());
                    contacto.setTelefono1(txtTel1.getText().toString());
                    contacto.setTelefono2(txtTel2.getText().toString());
                    if(chkFavorito.isChecked()){
                        contacto.setFavorito(1);
                    }else{
                        contacto.setFavorito(0);
                    }

                    db.openDataBase();

                    if(savedContact == null){
                        long idx = db.insertarContacto(contacto);
                        Toast.makeText(MainActivity.this, "Se agregó el contacto con el ID: "+ idx, Toast.LENGTH_SHORT).show();
                        Limpiar();
                    }else{
                        db.UpdateContacto(contacto, id);
                        Toast.makeText(MainActivity.this, "Se actualizó el Registro: "+ id, Toast.LENGTH_SHORT).show();
                        Limpiar();
                    }

                    db.cerrarBD();

                }else{
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean validar(){
        if(txtNotas.getText().toString().matches("") ||
            txtTel2.getText().toString().matches("") ||
            txtTel1.getText().toString().matches("") ||
            txtNombre.getText().toString().matches("") ||
            txtDomicilio.getText().toString().matches("")){
            return false;
        }else{
            return true;
        }
    }


    public void Limpiar(){
        txtDomicilio.setText("");
        txtNombre.setText("");
        txtNotas.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNombre.requestFocus();
        Toast.makeText(MainActivity.this, "¡Listo!", Toast.LENGTH_LONG).show();
    }
}
