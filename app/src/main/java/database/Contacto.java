package database;

import java.io.Serializable;

public class Contacto implements Serializable {
    private long ID;
    private  String nombre, telefono1, telefono2, domicilio, notas;
    private int favorito;

    public Contacto() {
    }

    public Contacto(Contacto kona) {
        this.ID = kona.ID;
        this.nombre = kona.nombre;
        this.telefono1 = kona.telefono1;
        this.telefono2 = kona.telefono2;
        this.domicilio = kona.domicilio;
        this.notas = kona.notas;
        this.favorito = kona.favorito;
    }


    public Contacto(long ID, String nombre, String telefono1, String telefono2, String domicilio, String notas, int favorito) {
        this.ID = ID;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.domicilio = domicilio;
        this.notas = notas;
        this.favorito = favorito;
    }


    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }
}
